package fr.blessedraccoon.minecraft.plugins.inventorygui;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class FixedItems implements Listener {

    private final List<ItemStack> fixedItems;
    private final Plugin plugin;

    public FixedItems(Plugin plugin) {
        this.fixedItems = new ArrayList<>();
        this.plugin = plugin;
    }

    /**
     * WARNING: this should only be done ONCE PER PLUGIN
     */
    public void activate() {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void addItem(ItemStack item) {
        this.fixedItems.add(item);
    }

    public List<ItemStack> getFixedItems() {
        return fixedItems;
    }

    public void removeItem(int index) {
        this.fixedItems.remove(index);
    }

    @EventHandler
    public void onInventoryMoving(InventoryClickEvent e) {
        if (this.fixedItems.contains(e.getCurrentItem())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDropEvent(PlayerDropItemEvent e) {
        if (this.fixedItems.contains(e.getItemDrop().getItemStack())) {
            e.setCancelled(true);
        }
    }
}

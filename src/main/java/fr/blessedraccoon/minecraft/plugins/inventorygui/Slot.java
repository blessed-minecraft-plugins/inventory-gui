package fr.blessedraccoon.minecraft.plugins.inventorygui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

public abstract class Slot {

    private ItemStack item;

    public Slot(@NotNull ItemStack item) {
        this.item = item;
    }

    @NotNull
    public ItemStack getItem() {
        return item;
    }

    public abstract void onClick(@NotNull Player player);
}

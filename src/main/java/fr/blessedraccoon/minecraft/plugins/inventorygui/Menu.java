package fr.blessedraccoon.minecraft.plugins.inventorygui;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

public class Menu implements Listener {

    private final Inventory inventory;
    private Slot[] slots;

    public Menu(Player player, String title, InventoryType type, Slot[] slots) {
        this(player, Component.text(title), type, slots);
    }

    public Menu(Player player, Component title, InventoryType type, Slot[] slots) {
        this.inventory = Bukkit.createInventory(player, type, title);
        this.slots = slots;

        for (int i = 0; i < slots.length; i++) {
            var slot = this.slots[i];
            if (slot != null) {
                this.inventory.setItem(i, slot.getItem());
            }
        }
    }

    public Slot[] getSlots() {
        return slots;
    }

    public void setSlots(Slot[] slots) {
        this.slots = slots;
    }

    public void setSlot(Slot slot, int i) {
        if (i < 0 || i > this.slots.length) return;

        this.slots[i] = slot;
    }

    public Slot getSlot(int i) {
        if (i < 0 || i > this.slots.length) return null;

        return this.slots[i];
    }

    public void freeze(@NotNull Plugin plugin) {
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void open(Player p) {
        p.openInventory(this.inventory);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        var player = (Player) e.getWhoClicked();
        if (e.getView().getTopInventory().equals(this.inventory)) {
            e.setCancelled(true);
            Slot slot = slots[e.getSlot()];
            slot.onClick(player);
        }
    }

}
